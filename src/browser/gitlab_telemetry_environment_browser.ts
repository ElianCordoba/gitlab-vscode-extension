import { GitLabTelemetryEnvironment } from '../common/platform/gitlab_telemetry_environment';
import { WebIDEExtension } from '../common/platform/web_ide';

export class GitLabTelemetryEnvironmentBrowser implements GitLabTelemetryEnvironment {
  readonly #webIdeExtension: WebIDEExtension | undefined;

  constructor(webIdeExtension: WebIDEExtension) {
    this.#webIdeExtension = webIdeExtension;
  }

  isTelemetryEnabled(): boolean {
    return this.#webIdeExtension?.isTelemetryEnabled() || false;
  }
}
